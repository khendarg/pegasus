#!/usr/bin/env python

import os
import re
import argparse
import xml.etree.ElementTree as ET
import tempfile

def parse_div(div):
	tokens = []
	for elem in div:
		if elem.tag == 'sp':
			for thing in elem: 
				if thing.tag == 'p': 
					if thing.text and thing.text.strip():
						obj = {}
						obj['type'] = 'splitline'
						tokens.append(obj)

						obj = {}
						obj['type'] = 'text'
						obj['value'] = thing.text.strip()
						obj['lineno'] = None
						tokens.append(obj)
					for subthing in thing: 
						if subthing.tail is None: line = ''
						else: line = subthing.tail
						if not line.strip(): continue
						lineno = subthing.attrib.get('n')
						obj = {}
						obj['type'] = 'text'
						obj['value'] = line.strip()
						obj['lineno'] = lineno
						tokens.append(obj)
					obj = {}
					obj['type'] = 'parbreak'
					tokens.append(obj)
				elif thing.tag == 'speaker':
					obj = {}
					obj['type'] = 'speaker'
					obj['value'] = thing.text
					tokens.append(obj)
		elif elem.tag.startswith('div'): tokens.extend(parse_div(elem))
						
						#print(subthing, subthing.text, subthing.tail)
	return tokens

def parse_xml(fh):

	tf = tempfile.TemporaryFile()
	s = fh.read()
	s = re.sub('</?del[^>]*>', '', s)
	tf.write(s.encode('utf-8'))
	tf.flush()
	tf.seek(0)

	parser = ET.parse(tf)

	tokens = []

	for text in parser.getroot():
		for body in text:
			for div in body:
				newtok = parse_div(div)
				#if not len(newtok):
				#	for subdiv in div:
				#		if subdiv.tag.startswith('div'): newtok = parse_div(subdiv)
				tokens.extend(newtok)

	obj = {}
	obj['type'] = 'cardbreak'
	tokens.append(obj)

	return tokens

def renumber_tokens(tokens):
	for token in tokens:
		if token['type'] == 'text':
			if token['lineno'] is None: token['lineno'] = '1'

def build_tex(tokens):
	out = ''
	out += r'''\begin{verse}
\poemlines{5}
'''
	for tno, token in enumerate(tokens):
		if token['type'] == 'text':
			if token['lineno']:
				try: 
					int(token['lineno'])
					out += r'\setcounter{vslineno}{' + token['lineno'] + '}\n'
					out += r'\setcounter{poemline}{' + token['lineno'] + '}\n'
				except ValueError: pass

			if (tno < (len(tokens) - 1)):
				#if (tno < (len(tokens) - 3)) and (tokens[tno+3]['type'] == 'splitline'): 
				#	#print(tno, token)
				#	out += r'\settowidth{\versewidth}{' + token['value'][:-8] + '} ' + token['value']
				#	out += r' \\>[\versewidth]' + '\n'

				if tokens[tno+1]['type'] == 'parbreak': 
					out += token['value'] 
					out += r' \\!' + '\n'

				else: 
					out += token['value'] 
					out += r' \\' + '\n'

			else: 
				out += token['value'] 
				out += r' \\' + '\n'

		elif token['type'] == 'speaker':
			out += r'\flagverse{\textsc{' + token['value'] + r'}}' + '\n'

		elif token['type'] == 'cardbreak':
			out += r'\vspace{12pt}\hrule ' + '\n'
	out += r'''\end{verse}'''	

	return out

def main(indir, outfn):
	tokens = []
	for fn in sorted(os.listdir(indir)):
		if fn.endswith('xml'):
			with open('{}/{}'.format(indir, fn)) as fh:
				newtok = parse_xml(fh)
				#print(fn, len(newtok))
				tokens.extend(newtok)
	renumber_tokens(tokens)
	texout = build_tex(tokens)
	with open(outfn, 'w') as fh:
		fh.write(texout)
	#for t in tokens: print(t)


if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('indir', help='Directory with all Perseus XMLs')
	parser.add_argument('-o', help='Where to save the resulting LaTeX')

	args = parser.parse_args()

	main(args.indir, args.o)
