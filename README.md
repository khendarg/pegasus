Usage
=====

1. Place all relevant XMLs into a single directory

2. Run scribe.py

```bash
python scribe.py -o orestes.tex orestes
```

3. Include the text in a document

```latex
...

\usepackage{verse}

...

\input{./orestes.tex}

...
```
